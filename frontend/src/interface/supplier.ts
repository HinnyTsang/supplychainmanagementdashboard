export interface ISupplierId {
    id: number;
}

export interface ISupplier {
    name: string;
    contact: string;
}

export interface ISupplierRecords extends ISupplier {
    id: number;
}

export interface ISupplierDeliveryInput {
    supplierId: number;
    productId: number;
    quantity: number;
}
