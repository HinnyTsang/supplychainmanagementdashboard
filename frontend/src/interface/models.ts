export type NativeLoginInput = {
    username: string;
    password: string;
};

export type NativeLoginOutput = {
    jwt_token: string;
};

export type GoogleLoginInput = {
    token: string;
};

export type GoogleLoginOutput = {
    jwt_token: string;
};
