export interface IProductContent {
    productName: string;
    quantity: number;
}

export interface IProduct extends IProductContent {
    id: number;
}

export interface INewProduct extends IProductContent {
    supplierId: number;
}

export interface IProductList {
    id: number;
    productName: string;
    warehouse: number;
    ordered: number;
    totalOrder: number;
    exitedFactory: number;
    arrivedOutboundAirport: number;
    arrivedInboundAirport: number;
    arrivedLocalWarehouse: number;
    calledCustomerPick: number;
    arrivedCustomerAddress: number;
    customerPickedUp: number;
}
