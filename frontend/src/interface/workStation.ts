export interface IWorkstationList {
    id: number;
    productName: string;
    invoiceNo: string;
    updateAt: Date;
    arrivalTime: Date;
}

export interface IProcessNextWorkStation {
    selectedShipmentIds: number[];
    currentWorkstation: string;
}

export const workStation = {
    warehouse: 'warehouse',
    exitedFactory: 'exitedFactory',
    arrivedOutboundAirport: 'arrivedOutBoundAirport',
    arrivedInboundAirport: 'arrivedInboundAirport',
    arrivedLocalWarehouse: 'arrivedLocalWarehouse',
    calledCustomerPick: 'calledCustomerPick',
    arrivedCustomerAddress: 'arrivedCustomerAddress',
    customerPickedUp: 'customerPickedUp',
};
