export interface ICustomerOrderInput {
    customerName: string;
    customerAddress: string;
    productId: number;
    quantity: number;
}
