// redux
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';

interface Item {
    id: number,
    name: string,
    price: number
}

const TodoList = () => {
    const todoList = useSelector((state: RootState) => state.todoList.todoList)

    return (
    <div>
        <div>This is item list page</div>
        {todoList.map((item:Item,i) => (
            <div key={i}>
                <div>{item.id}: {item.name} - ${item.price}</div>
            </div>
        ))}
    </div>
    )
}

export default TodoList