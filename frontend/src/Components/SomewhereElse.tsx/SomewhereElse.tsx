import React, { useState } from 'react';

// redux
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { useDispatch } from 'react-redux';

// action
import { todoListAction } from '../../redux/todoList'

// type
import { Item } from '../../redux/todoList'

const SomewhereElse = () => {
    const dispatch = useDispatch();

    const [name, setName] = useState<string>('');
    const [price, setPrice] = useState<number>(0);

    const todoList = useSelector((state: RootState) => state.todoList.todoList)

    const submitForm = (e: React.FormEvent) => {
        if (!name) {
            return;
        }
        if (!price) {
            return;
        }
        e.preventDefault();
        console.log({
            name,
            price,
        });
        setName('');
        setPrice(0);
        const newList:Item[] = ([...todoList, {id: todoList.length, name, price}])
        dispatch(todoListAction(newList))
        alert('success')
    };

    return (
        <div>
            <form onSubmit={submitForm}>
                <label>
                    Name:
                    <input
                        type="text"
                        name="name"
                        value={name}
                        onChange={(e: React.FormEvent<HTMLInputElement>) =>
                            setName(e.currentTarget.value)
                        }
                    />
                </label>
                <label>
                    Price:
                    <input
                        type="number"
                        name="price"
                        value={price}
                        onChange={(e: React.FormEvent<HTMLInputElement>) =>
                            setPrice(+e.currentTarget.value)
                        }
                    />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    );
};

export default SomewhereElse;
