import { Link } from 'react-router-dom';

type LinkButtonProps = {
    name: string;
    url: string;
};

const LinkButton = ({ name, url }: LinkButtonProps) => {
    return (
        <Link to={url} style={{ color: 'white', textDecoration: 'none' }}>
            {name}
        </Link>
    );
};

export default LinkButton;
