import './NewSupplierForm.scss';
import { useEffect, useState } from 'react';

// css library
import Button from '@mui/material/Button';
import Form from 'react-bootstrap/Form';

// react hook form
import { useForm, SubmitHandler } from 'react-hook-form';

// sweet alert 2
import Swal from 'sweetalert2';

// react router dom
import { BrowserRouter as Router, useParams, useNavigate } from 'react-router-dom';

// interface
import { ISupplier, ISupplierRecords, ISupplierId } from '../../interface/supplier';
import { callAPI } from '../../CallAPI';

// useage
// This Form is used for editing supplier information

const EditSupplierForm = () => {
    let { id } = useParams();
    let navigate = useNavigate();
    const [supplier, setSupplier] = useState<ISupplier>({ name: '', contact: '' });
    const { register, handleSubmit } = useForm<ISupplier>();

    const onSubmit: SubmitHandler<ISupplier> = (data) => {
        if ((data.name && data.contact) || data.name || data.contact) {
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in atleast one field',
            });
            return;
        }

        if (data.name && data.name.length > 100) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of supplier',
            });
            return;
        }

        if (data.contact && data.contact.length > 20) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of contact',
            });
            return;
        }

        if (!id) {
            return;
        }

        callAPI<ISupplierRecords, string>('PUT', `/supplier`, {
            id: +id,
            name: data.name,
            contact: data.contact,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(async () => {
                await callAPI<null, ISupplier>('GET', `/supplier/${id}`).then((data) =>
                    setSupplier(data),
                );
            });
    };

    useEffect(() => {
        callAPI<null, ISupplier>('GET', `/supplier/${id}`).then((data) => setSupplier(data));
    }, []);

    const deleteSupplier = () => {
        if (!id) {
            return;
        }

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        }).then((result) => {
            if (result.isConfirmed && id) {
                callAPI<ISupplierId, string>('DELETE', '/supplier', {
                    id: +id,
                })
                    .then(() => {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'supplier delete successfully',
                        });
                    })
                    .catch((err) => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: err.message,
                        });
                    })
                    .then(() => {
                        navigate('/supplier/');
                    });
            }
        });
    };

    return (
        <div className="newSupplierForm">
            <div className="title">Edit Supplier</div>
            <div className="formContent">
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Supplier Name: {supplier.name}</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="fill in update info if needed"
                            {...register('name')}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Supplier Contact: {supplier.contact}</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="fill in update info if needed"
                            {...register('contact')}
                        />
                    </Form.Group>
                    <Button variant="contained" type="submit" className="button">
                        Edit
                    </Button>
                    <Button
                        variant="contained"
                        type="button"
                        onClick={deleteSupplier}
                        className="button"
                    >
                        Delete
                    </Button>
                </Form>
            </div>
        </div>
    );
};

export default EditSupplierForm;
