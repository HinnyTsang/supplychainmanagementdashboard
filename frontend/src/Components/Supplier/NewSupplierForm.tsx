import './NewSupplierForm.scss';

// css library
import Button from '@mui/material/Button';

// react hook form
import { useForm, SubmitHandler } from 'react-hook-form';

// sweet alert 2
import Swal from 'sweetalert2';

import Form from 'react-bootstrap/Form';

// interface
import { ISupplier } from '../../interface/supplier';
import { callAPI } from '../../CallAPI';

// react router dom
import { BrowserRouter as Router, useNavigate } from 'react-router-dom';

// useage
// This Form is used for Adding New Supplier

const NewSupplierForm = () => {
    let navigate = useNavigate();
    const { register, handleSubmit } = useForm<ISupplier>();

    const onSubmit: SubmitHandler<ISupplier> = (data) => {
        if (!data.name) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in supplier name',
            });
            return;
        }

        if (data.name.length > 100) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of supplier',
            });
            return;
        }

        if (!data.contact) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in supplier contact',
            });
            return;
        }

        if (data.contact.length > 20) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid length of contact',
            });
            return;
        }

        callAPI<ISupplier, string>('POST', '/supplier', {
            name: data.name,
            contact: data.contact,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(() => {
                navigate('/supplier/');
            });
    };

    return (
        <div className="newSupplierForm">
            <div className="title">New Supplier</div>
            <div className="formContent">
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Supplier Name</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="please fill in supplier Name"
                            {...register('name')}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Supplier Contact</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="please fill in supplier Contact"
                            {...register('contact')}
                        />
                    </Form.Group>
                    <Button variant="contained" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        </div>
    );
};

export default NewSupplierForm;
