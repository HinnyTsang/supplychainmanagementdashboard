import { useEffect, useState } from 'react';
import '../Product/NewProductForm.scss';

// css library
import Button from '@mui/material/Button';
import Form from 'react-bootstrap/Form';

// react hook form
import { useForm, SubmitHandler } from 'react-hook-form';

// sweet alert 2
import Swal from 'sweetalert2';

// interface
import { IProduct } from '../../interface/product';
import { ISupplierRecords, ISupplierDeliveryInput } from '../../interface/supplier';

// Fetch function
import { callAPI } from '../../CallAPI';

// react router dom
import { BrowserRouter as Router, useNavigate } from 'react-router-dom';

// useage
// This Form is used for Adding New Supplier

const DeliveryForm = () => {
    let navigate = useNavigate();
    const { register, handleSubmit } = useForm<ISupplierDeliveryInput>();
    const [suppliers, setSuppliers] = useState<ISupplierRecords[]>([]);
    const [products, setProducts] = useState<IProduct[]>([]);

    useEffect(() => {
        callAPI<null, ISupplierRecords[]>('GET', '/supplier')
            .then((data) => setSuppliers(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );

        callAPI<null, IProduct[]>('GET', '/product')
            .then((data) => setProducts(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );
    }, []);

    const onSubmit: SubmitHandler<ISupplierDeliveryInput> = (data) => {
        const supplierInfo = data.supplierId.toString();
        const supplierId = +supplierInfo.split('-')[0];

        const productInfo = data.productId.toString();
        const productId = +productInfo.split('-')[0];

        if (isNaN(supplierId)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please select a supplier',
            });
            return;
        }

        if (isNaN(productId)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please select a product',
            });
            return;
        }

        if (!data.quantity) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'please fill in supplier contact',
            });
            return;
        }

        if (data.quantity > 1000) {
            Swal.fire({
                icon: 'error',
                title: 'Oops',
                text: 'invalid quantity of product',
            });
            return;
        }

        callAPI<ISupplierDeliveryInput, string>('POST', '/delivery-form-supplier', {
            supplierId: supplierId,
            productId: productId,
            quantity: +data.quantity,
        })
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'success',
                    text: 'supplier added successfully',
                });
            })
            .catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops',
                    text: err.message,
                });
            })
            .then(() => {
                navigate('/stock-management');
            });
    };

    return (
        <div className="newProductForm">
            <div className="title">New Deliver</div>
            <div className="formContent">
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-3">
                        <Form.Label>Select Supplier</Form.Label>
                        <Form.Select {...register('supplierId')}>
                            <option key={0}>Please Select Supplier</option>
                            {suppliers.map((supplier) => (
                                <option key={supplier.id}>
                                    {supplier.id + '-' + supplier.name}
                                </option>
                            ))}
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Select Product</Form.Label>
                        <Form.Select {...register('productId')}>
                            <option key={0}>Please Select Product</option>
                            {products.map((product) => (
                                <option key={product.id}>
                                    {product.id + '-' + product.productName}
                                </option>
                            ))}
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Deliver Quantity</Form.Label>
                        <Form.Control
                            type="number"
                            min={0}
                            placeholder="please fill in opening balance"
                            {...register('quantity')}
                        />
                    </Form.Group>
                    <Button variant="contained" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        </div>
    );
};

export default DeliveryForm;
