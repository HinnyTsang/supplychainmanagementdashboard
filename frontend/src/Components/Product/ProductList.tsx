import { useState, useEffect } from 'react';

import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import TableHead from '@mui/material/TableHead';
import Button from '@mui/material/Button';

// function
import { callAPI } from '../../CallAPI';
import Swal from 'sweetalert2';

// interface
import { IProductList } from '../../interface/product';

// react router dom
import { useNavigate } from 'react-router-dom';

// useage
// This components shows all suppliers in /supplier page.

interface TablePaginationActionsProps {
    count: number;
    page: number;
    rowsPerPage: number;
    onPageChange: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

function TablePaginationActions(props: TablePaginationActionsProps) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

const ProductList = () => {
    let navigate = useNavigate();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [product, setProduct] = useState<IProductList[]>([]);

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - product.length) : 0;

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const switchPage = (productId: number) => {
        console.log(productId);
        console.log(product);
        // navigate(`/product/${supplierId}`);
    };

    useEffect(() => {
        callAPI<null, IProductList[]>('GET', '/product')
            .then((data) => setProduct(data))
            .catch((err) =>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: err.message,
                }),
            );
    }, []);

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                <TableHead>
                    <TableRow>
                        <TableCell>Product Name</TableCell>
                        <TableCell align="right">Ordered(Total)</TableCell>
                        <TableCell align="right">Ordered(On the Way)</TableCell>
                        <TableCell align="right">Warehouse</TableCell>
                        <TableCell align="right">Exited Factory</TableCell>
                        <TableCell align="right">Arrived Outbound Airport</TableCell>
                        <TableCell align="right">Arrived Inbound Airport</TableCell>
                        <TableCell align="right">Arrived Local Warehouse</TableCell>
                        <TableCell align="right">Call Customer to pick up</TableCell>
                        <TableCell align="right">Arrived Customer Address</TableCell>
                        <TableCell align="right">Customer picked up</TableCell>
                        <TableCell align="right"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? product.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : product
                    ).map((productDetails: IProductList, i) => (
                        <TableRow key={i}>
                            <TableCell component="th" scope="row">
                                {productDetails.productName}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.totalOrder}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.ordered}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.warehouse}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.exitedFactory}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.arrivedOutboundAirport}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.arrivedInboundAirport}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.arrivedLocalWarehouse}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.calledCustomerPick}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.arrivedCustomerAddress}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {productDetails.customerPickedUp}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                <Button variant="outlined">
                                    <div onClick={() => switchPage(productDetails.id)}>Edit</div>
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                        </TableRow>
                    )}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                            colSpan={3}
                            count={product.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: {
                                    'aria-label': 'rows per page',
                                },
                                native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

export default ProductList;
