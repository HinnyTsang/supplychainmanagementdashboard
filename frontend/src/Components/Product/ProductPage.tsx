import './ProductPage.scss';

// css library
import Button from '@mui/material/Button';

// react roter
import { Link } from 'react-router-dom';

// components
import ProductList from './ProductList';

const StockManagement = () => {
    return (
        <div className="stockManagement">
            <div className="buttonGroup">
                <Button variant="contained" className="button">
                    <Link style={{ textDecoration: 'none', color: '#FFF' }} to="/new-product">
                        New Product
                    </Link>
                </Button>
            </div>
            <div className="table">
                <ProductList />
            </div>
        </div>
    );
};

export default StockManagement;
