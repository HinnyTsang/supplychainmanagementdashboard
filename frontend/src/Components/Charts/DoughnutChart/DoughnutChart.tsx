import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import './DoughnutChart.scss';

ChartJS.register(ArcElement, Tooltip, Legend);

type IDoughnutChartProps = {
    title: string;
    data1: number;
    data2: number;
};

const DoughnutChart = ({ title, data1, data2 }: IDoughnutChartProps) => {
    const data = {
        datasets: [
            {
                data: [data1, data2],
                backgroundColor: ['rgba(51, 153, 255, 1)', 'rgba(255,128,0, 1)'],
                borderColor: ['rgba(51, 153, 255, 1)', 'rgba(255,128,0, 1)'],
                borderWidth: 1,
            },
        ],
        text: '24',
    };

    return (
        <div className="doughnutChart">
            <div className="title">{title}</div>
            <div className="chart">
                <Doughnut data={data} width={200} height={200} />
                <div className="middleText">
                    <span>{data1}</span>
                </div>
            </div>
        </div>
    );
};

export default DoughnutChart;
