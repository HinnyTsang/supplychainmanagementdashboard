import { useEffect, useState } from 'react';

// css framework
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

// redux
import { useDispatch } from 'react-redux';
import { nativeLoginThunk } from '../../redux';
import { loginSuccessAction, logoutAction } from '../../redux/auth/action';

// google login
import GoogleLogin from 'react-google-login';

// fetch function
import { callAPI } from '../../CallAPI';

// interface
import { GoogleLoginInput, GoogleLoginOutput } from '../../interface/models';

const theme = createTheme();

export default function SignIn() {
    const dispatch = useDispatch();

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        const username = data.get('username')?.toString();
        const password = data.get('password')?.toString();

        if (!username) {
            return;
        }

        if (!password) {
            return;
        }

        dispatch(nativeLoginThunk(username, password));
    };

    const handleGoogleLogin = async (res: any) => {
        await callAPI<GoogleLoginInput, GoogleLoginOutput>('POST', '/google-login', {
            token: res.tokenId,
        }).then((data) => {
            localStorage.setItem('token', data.jwt_token);
            dispatch(loginSuccessAction(data.jwt_token));
        });
    };

    const handleGoogleLogout = (res: any) => {
        dispatch(logoutAction())
    };

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="使用者名稱"
                            name="username"
                            autoComplete="username"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <Grid container sx={{ mt: 1, mb: 1 }} color="red">
                            {/* {'Some Warning Message'} */}
                        </Grid>
                        <GoogleLogin
                            clientId={
                                '889072860592-008b46525mha8kqbuhqma479ll7ovuvc.apps.googleusercontent.com'
                            }
                            buttonText="Log in with Google"
                            onSuccess={handleGoogleLogin}
                            onFailure={handleGoogleLogout}
                            cookiePolicy={'single_host_origin'}
                        ></GoogleLogin>
                        <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                            Sign In
                        </Button>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}
