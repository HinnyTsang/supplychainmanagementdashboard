import './WorkStation.scss';

// component
import DeliveryStatusList from './DeliveryStatusList';

const WorkStation = () => {
    return (
        <div className="workStation">
            <div className="table">
                <DeliveryStatusList />
            </div>
        </div>
    );
};

export default WorkStation;
