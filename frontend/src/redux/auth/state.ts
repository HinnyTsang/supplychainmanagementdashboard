import { JWTPayload } from '../../interface/gobalState';

export interface AuthState {
    isAuthenticated: boolean;
    errorMessage?: string;
    payload?: JWTPayload;
}
