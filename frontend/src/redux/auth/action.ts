export function loginSuccessAction(token: string) {
    return {
        type: '@@auth/login_success' as const,
        token,
    };
}

export function loginFailAction(errorMessage: string) {
    return {
        type: '@@auth/login_fail' as const,
        errorMessage,
    };
}

export function logoutAction() {
    return {
        type: '@@auth/logout' as const,
    };
}

export type AuthAction =
    | ReturnType<typeof loginSuccessAction>
    | ReturnType<typeof loginFailAction>
    | ReturnType<typeof logoutAction>;
