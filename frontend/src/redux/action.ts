import { CallHistoryMethodAction } from 'connected-react-router';
import { AuthAction } from './auth/action';
import { TodoListAction } from './todoList';

export type RootAction = CallHistoryMethodAction | AuthAction | TodoListAction;
