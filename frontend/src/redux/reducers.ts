import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { authReducer } from './auth/reducer';
import { todoListReducer } from './todoList';
import { history } from './history';

export let rootReducer = combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    todoList: todoListReducer
});
