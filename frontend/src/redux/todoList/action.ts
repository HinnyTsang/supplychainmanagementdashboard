import { Item } from './state'

export function todoListAction(todoList:Item[]){
    return {
        type: '@@todoList/addItem' as const,
        todoList
    }
}

export type TodoListAction = 
    | ReturnType<typeof todoListAction>