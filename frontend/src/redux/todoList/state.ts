export interface Item {
    id: number,
    name: string,
    price: number
}

export interface TodoListState {
    todoList: Item[]
}