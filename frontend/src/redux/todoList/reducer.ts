import { TodoListAction } from "./action";
import { TodoListState } from "./state";

const initialState: TodoListState ={
    todoList: [{id: 1, name: 'toy', price: 3.99}, {id:2, name: 'apple', price: 5.99}]
}

export const todoListReducer = (state:TodoListState= initialState, action: TodoListAction):TodoListState => {
    switch (action.type){
        case '@@todoList/addItem': {
            return {
                todoList: action.todoList
            }
        }
        default:
            return state
    }
}