import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { history } from './history';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers';

let rootEnhancer = compose(applyMiddleware(thunk), applyMiddleware(routerMiddleware(history)));

export let store = createStore(rootReducer, rootEnhancer);
