// css
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Layout
import ResponsiveAppBar from './Components/Layout/Header';

// Router component
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// redux
import { useSelector } from 'react-redux';
import { RootState } from './redux';

// components
import HomePage from './Components/HomePage/HomePage';
import Supplier from './Components/Supplier/Supplier';
import SignIn from './Components/Auth/SignIn';
import ProductPage from './Components/Product/ProductPage';
import EditSupplierForm from './Components/Supplier/EditSupplierForm';
import NewSupplierForm from './Components/Supplier/NewSupplierForm';
import DeliveryForm from './Components/Supplier/NewDeliveryForm';
import NewProductForm from './Components/Product/NewProductForm';
import CustomerOrderForm from './Components/Order/CustomerOrderForm';
import WorkStation from './Components/WorkStation/WorkStation';
import TodoList from './Components/TodoList/TodoList';
import SomewhereElse from './Components/SomewhereElse.tsx/SomewhereElse'

// default functions
function App() {
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);

    return (
        <div>
            {isAuthenticated ? (
                <div>
                    <ResponsiveAppBar />

                    {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                    <Routes>
                        <Route path="/" element={<HomePage />} />
                        <Route path="/supplier" element={<Supplier />} />
                        <Route path="/stock-management" element={<ProductPage />} />
                        <Route path="/supplier/:id" element={<EditSupplierForm />} />
                        <Route path="/new-supplier" element={<NewSupplierForm />} />
                        <Route path="/new-delivery" element={<DeliveryForm />} />
                        <Route path="/new-product" element={<NewProductForm />} />
                        <Route path="/new-order" element={<CustomerOrderForm />} />
                        <Route path="/work-station" element={<WorkStation />} />
                        <Route path="/todoList" element={<TodoList />} />
                        <Route path="/somewhere-else" element={<SomewhereElse />} />
                    </Routes>
                </div>
            ) : (
                <SignIn />
            )}
        </div>
    );
}

export default App;
