const tables = Object.freeze({
    USERS: 'users',
    PRODUCT: 'product',
    SHIPPMENT: 'shippment',
    INVOICE: 'invoice',
    SUPPLIER: 'supplier',
});

export const workStation = {
    warehouse: 'warehouse',
    exitedFactory: 'exitedFactory',
    arrivedOutboundAirport: 'arrivedOutBoundAirport',
    arrivedInboundAirport: 'arrivedInboundAirport',
    arrivedLocalWarehouse: 'arrivedLocalWarehouse',
    calledCustomerPick: 'calledCustomerPick',
    arrivedCustomerAddress: 'arrivedCustomerAddress',
    customerPickedUp: 'customerPickedUp',
};

export default tables;
