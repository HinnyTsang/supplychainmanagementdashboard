export interface IDeliveryFromSupplierInput {
    supplierId: number;
    productId: number;
    quantity: number;
}

export interface workStationListInput {
    workStation: string;
}

export interface workStationList {
    id: number;
    productName: string;
    invoiceNo: number;
    updatedAt: Date;
}

export interface IProcessNextWorkStation {
    selectedShipmentIds: number[];
    currentWorkstation: string;
}
