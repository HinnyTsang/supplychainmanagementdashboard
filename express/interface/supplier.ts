export interface ISupplier {
    name: string;
    contact: string;
}

export interface IUpdateSupplier {
    id: number;
    name?: string;
    contact?: string;
}
