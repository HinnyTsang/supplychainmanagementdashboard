import Knex from 'knex';

let configs = require('../knexfile');
let config = configs.development;

export let knex = Knex(config);
