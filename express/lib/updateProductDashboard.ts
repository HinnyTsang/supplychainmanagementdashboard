import { knex } from "./db"

// database tables
import tables from '../interface/tables';

// interface
import { IReadProduct} from '../interface/product';

export const updateProductDashboard = async() => {
    const result: IReadProduct[] = [];

    const productList = await knex(tables.SHIPPMENT)
        .join(tables.PRODUCT, 'shippment.product_id', 'product.id')
        .select('product_name', 'product_id')
        .groupBy('product_name');

    for (const product of productList) {
        const warehouse = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNull('exited_factory');

        const totalOrder = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id');

        const ordered = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNull('customer_picked_up');

        const exitedFactory = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('exited_factory')
            .whereNull('arrived_outbound_airport');

        const arrivedOutboundAirport = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('arrived_outbound_airport')
            .whereNull('arrived_inbound_airport');

        const arrivedInboundAirport = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('arrived_inbound_airport')
            .whereNull('arrived_local_warehouse');

        const arrivedLocalWarehouse = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('arrived_local_warehouse')
            .whereNull('called_customer_pick');

        const calledCustomerPick = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('called_customer_pick')
            .whereNull('arrived_customer_address');

        const arrivedCustomerAddress = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('arrived_customer_address')
            .whereNull('customer_picked_up');

        const customerPickedUp = await knex(tables.SHIPPMENT)
            .count('product_id', product.product_id)
            .where('product_id', product.product_id)
            .whereNotNull('invoice_id')
            .whereNotNull('customer_picked_up');

        const data = {
            id: product.product_id,
            productName: product.product_name,
            warehouse: +warehouse[0]['count(`product_id`)'],
            totalOrder: +totalOrder[0]['count(`product_id`)'],
            ordered: +ordered[0]['count(`product_id`)'],
            exitedFactory: +exitedFactory[0]['count(`product_id`)'],
            arrivedOutboundAirport: +arrivedOutboundAirport[0]['count(`product_id`)'],
            arrivedInboundAirport: +arrivedInboundAirport[0]['count(`product_id`)'],
            arrivedLocalWarehouse: +arrivedLocalWarehouse[0]['count(`product_id`)'],
            calledCustomerPick: +calledCustomerPick[0]['count(`product_id`)'],
            arrivedCustomerAddress: +arrivedCustomerAddress[0]['count(`product_id`)'],
            customerPickedUp: +customerPickedUp[0]['count(`product_id`)'],
        };

        result.push(data);
    }

    return result
}