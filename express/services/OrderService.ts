import { Knex } from 'knex';

// database tables
import tables from '../interface/tables';

// interface
import { ICustomerOrderInput } from '../interface/order';

// throw error function
import { HttpException } from '../base-controller';

// redis
import { client } from '../lib/redis';

// updateProductDashboard function
import { updateProductDashboard } from '../lib/updateProductDashboard'

export class OrderService {
    constructor(public knex: Knex) {}

    async create(
        { customerName, customerAddress, productId, quantity }: ICustomerOrderInput,
        knex = this.knex,
    ): Promise<string> {
        const today = new Date();
        const receivedOrder = await knex.transaction(async (trx) => {
            const customerOrder = await trx(tables.INVOICE).insert({
                customer_name: customerName,
                customer_address: customerAddress,
            });

            if (!customerOrder) {
                throw new HttpException(500, 'Internal Server Error');
            }

            const stockIds = await trx(tables.SHIPPMENT)
                .select('id')
                .where('product_id', productId)
                .whereNull('received_order')
                .limit(quantity);

            for (const stockId of stockIds) {
                try {
                    await trx(tables.SHIPPMENT)
                        .update({
                            invoice_id: customerOrder[0],
                            received_order: today,
                        })
                        .where('id', stockId.id);
                } catch (err) {
                    throw new HttpException(500, 'Internal Server Error');
                }
            }

            return 'Order Saved Succussfully';
        });

        const result = await updateProductDashboard()
        
        await client.connect().catch(error => {})
        await client.setEx('product-dashboard', 600, JSON.stringify(result))
        await client.quit()

        return receivedOrder;
    }
}
