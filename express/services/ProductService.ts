import { Knex } from 'knex';

// database tables
import tables from '../interface/tables';

// interface
import { IReadProduct, IProductInput } from '../interface/product';

// throw error funtion
import { HttpException } from '../base-controller';

// redis
import { client } from '../lib/redis';

// updateProductDashboard function
import { updateProductDashboard } from '../lib/updateProductDashboard'

export class ProductService {
    constructor(public knex: Knex) {}

    async create(
        { productName, quantity, supplierId }: IProductInput,
        knex = this.knex,
    ): Promise<string> {
        const today = new Date();

        const createProductTransaction = await knex.transaction(async (trx) => {
            const findProduct = await knex(tables.PRODUCT)
                .select('id')
                .where('product_name', productName)
                .first();

            if (findProduct) {
                throw new HttpException(400, 'Product Exit');
            }

            const findSupplier = await knex(tables.SUPPLIER)
                .select('id')
                .where('id', supplierId)
                .first();

            if (!findSupplier) {
                throw new HttpException(400, 'Supplier Not Found');
            }

            const createProduct = await trx(tables.PRODUCT).insert({
                product_name: productName,
                status: 'launched',
            });

            if (!createProduct) {
                throw new HttpException(500, 'Internal Server Error');
            }

            const productId = await trx(tables.PRODUCT)
                .select('id')
                .where('product_name', productName)
                .first();

            for (let i = 0; i < quantity; i++) {
                await trx(tables.SHIPPMENT).insert([
                    {
                        product_id: productId['id'],
                        supplier_id: supplierId,
                        arrived_warehouse: today,
                    },
                ]);
            }

            return 'Product Created Successfully';
        });

        const result = await updateProductDashboard()
        await client.connect().catch(error => {})
        await client.setEx('product-dashboard', 600, JSON.stringify(result))
        await client.quit()

        return createProductTransaction;
    }

    async read(knex = this.knex): Promise<IReadProduct[]> {
        await client.connect().catch(error => {})

        const value = await client.get('product-dashboard');
        if(value && value?.length > 0 ){
            client.quit()
            return JSON.parse(value)
        }

        const result = await updateProductDashboard()

        await client.setEx('product-dashboard', 600, JSON.stringify(result))
        await client.quit()
        return result;
    }
}
