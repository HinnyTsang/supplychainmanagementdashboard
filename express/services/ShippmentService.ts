import { Knex } from 'knex';

// database tables
import tables, { workStation as workStationInput } from '../interface/tables';

// throw error funtion
import { HttpException } from '../base-controller';
import {
    IDeliveryFromSupplierInput,
    workStationListInput,
    workStationList,
    IProcessNextWorkStation,
} from '../interface/shippment';

export class ShippmentService {
    constructor(public knex: Knex) {}

    // async allRecords(knex = this.knex): Promise<any[]> {
    //     return await knex(tables.SHIPPMENT).select('*');
    // }

    async getWorkstationList(
        { workStation }: workStationListInput,
        knex = this.knex,
    ): Promise<workStationList[]> {
        switch (workStation) {
            case workStationInput.exitedFactory:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('exited_factory')
                    .whereNull('arrived_outbound_airport')
                    .whereNotNull('invoice_id');
            case workStationInput.arrivedOutboundAirport:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('arrived_outbound_airport')
                    .whereNull('arrived_inbound_airport')
                    .whereNotNull('invoice_id');
            case workStationInput.arrivedInboundAirport:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('arrived_inbound_airport')
                    .whereNull('arrived_local_warehouse')
                    .whereNotNull('invoice_id');
            case workStationInput.arrivedLocalWarehouse:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('arrived_local_warehouse')
                    .whereNull('called_customer_pick')
                    .whereNotNull('invoice_id');
            case workStationInput.calledCustomerPick:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('called_customer_pick')
                    .whereNull('arrived_customer_address')
                    .whereNotNull('invoice_id');
            case workStationInput.arrivedCustomerAddress:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('arrived_customer_address')
                    .whereNull('customer_picked_up')
                    .whereNotNull('invoice_id');
            case workStationInput.customerPickedUp:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNotNull('customer_picked_up')
                    .whereNotNull('invoice_id');
            default:
                return await knex(tables.SHIPPMENT)
                    .join(tables.PRODUCT, 'product.id', 'shippment.product_id')
                    .select(
                        'shippment.id',
                        'product_name as productName',
                        'invoice_id as invoiceNo',
                        'shippment.updated_at as updatedAt',
                        'shippment.updated_at as arrivalTime',
                    )
                    .whereNull('exited_factory')
                    .whereNotNull('invoice_id');
        }
    }

    async processNextWorkStation(
        { currentWorkstation, selectedShipmentIds }: IProcessNextWorkStation,
        knex = this.knex,
    ): Promise<string> {
        let current = new Date();
        switch (currentWorkstation) {
            case workStationInput.warehouse:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ exited_factory: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.exitedFactory:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ arrived_outbound_airport: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.arrivedOutboundAirport:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ arrived_inbound_airport: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.arrivedInboundAirport:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ arrived_local_warehouse: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.arrivedLocalWarehouse:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ called_customer_pick: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.calledCustomerPick:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ arrived_customer_address: current })
                        .where('id', selectedId);
                }
                return `Selected task in ${currentWorkstation} is process successully`;
            case workStationInput.arrivedCustomerAddress:
                for (const selectedId of selectedShipmentIds) {
                    await knex(tables.SHIPPMENT)
                        .update({ customer_picked_up: current })
                        .where('id', selectedId);
                }
            default:
                throw new HttpException(500, 'Internal Server Error');
        }
    }

    async deliveryGoodsFromSupplier(
        { supplierId, productId, quantity }: IDeliveryFromSupplierInput,
        knex = this.knex,
    ): Promise<string> {
        const today = new Date();
        const deliveryFromSupplier = await knex.transaction(async (trx) => {
            for (let i = 0; i < quantity; i++) {
                try {
                    await trx(tables.SHIPPMENT).insert([
                        {
                            product_id: productId,
                            supplier_id: supplierId,
                            arrived_warehouse: today,
                        },
                    ]);
                } catch (err) {
                    throw new HttpException(500, 'Internal Server Error');
                }
            }

            return 'Deliver Records save succussfully';
        });

        return deliveryFromSupplier;
    }
}
