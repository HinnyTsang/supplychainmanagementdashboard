import { Knex } from 'knex';

// database tables
import tables from '../interface/tables';

// interface
import { ISupplier, IUpdateSupplier } from '../interface/supplier';

// function
import { HttpException } from '../base-controller';

export class SupplierService {
    constructor(public knex: Knex) {}

    async create({ name, contact }: ISupplier, knex = this.knex): Promise<string> {
        const exitSupplier = await knex(tables.SUPPLIER).select('id').where('name', name).first();

        if (exitSupplier) {
            throw new HttpException(400, 'Supplier Exit.');
        }

        const createSupplier = await knex(tables.SUPPLIER).insert({
            name,
            contact,
        });

        if (!createSupplier) {
            throw new HttpException(500, 'Internal Service Error.');
        }

        return 'Supplier create successfully';
    }

    async update({ id, name, contact }: IUpdateSupplier, knex = this.knex): Promise<string> {
        const updateSupplier = await knex(tables.SUPPLIER)
            .update({
                ...(name ? { name: name } : {}),
                ...(contact ? { contact: contact } : {}),
            })
            .where('id', id);

        if (!updateSupplier) {
            throw new HttpException(500, 'Internal Service Error.');
        }

        return 'Supplier Info update successfully';
    }

    async read(knex = this.knex): Promise<ISupplier[]> {
        const allSupplier = await knex(tables.SUPPLIER).select('id', 'name', 'contact');

        if (!allSupplier) {
            throw new HttpException(500, 'Internal Service Error.');
        }

        return allSupplier;
    }

    async readSingle(id: number, knex = this.knex): Promise<ISupplier> {
        const singleSupplier = await knex(tables.SUPPLIER).select('*').where('id', id).first();

        if (!singleSupplier) {
            throw new HttpException(500, 'Not found supplier.');
        }

        return singleSupplier;
    }

    async delete(id: number, knex = this.knex): Promise<string> {
        const deleteSupplier = await knex(tables.SUPPLIER).where('supplier.id', id).delete();

        if (!deleteSupplier) {
            throw new HttpException(500, 'No such supplier.');
        }

        return 'The supplier has deleted successfully';
    }
}
