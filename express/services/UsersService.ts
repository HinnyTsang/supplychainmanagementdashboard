import { Knex } from 'knex';
import tables from '../interface/tables';

// function
import { HttpException } from '../base-controller';
import jwtSimple from 'jwt-simple';
import jwt from '../lib/jwt';
import { hashPassword } from '../lib/hash';

// interface
import { checkPassword } from '../lib/hash';
import { JWTPayload } from '../interface/models';
import { NativeLoginInput, NativeLoginOutput, GoogleLoginUserInfo } from '../interface/models';

export class UsersService {
    constructor(public knex: Knex) {}

    async googleLogin(
        { name, email, picture }: GoogleLoginUserInfo,
        knex = this.knex,
    ): Promise<NativeLoginOutput> {
        const registerEmail = await knex(tables.USERS)
            .select('id', 'username', 'email')
            .where('email', email);

        console.log(registerEmail);

        if (!jwt.jwtSecret) {
            throw new HttpException(500, "Internal Service Error - missing 'JWT_Secret' in env.");
        }

        if (!registerEmail) {
            const hashedPassword = await hashPassword('skfjdgoisfdj45465s4dfg');
            const createUser = await knex(tables.USERS)
                .insert({
                    username: name,
                    email,
                    password: hashedPassword,
                })
                .returning('id');

            if (!createUser) {
                throw new HttpException(500, 'Internal Server Error');
            }

            let payload: JWTPayload = {
                user_id: registerEmail['id'] || createUser['id'],
                username: name,
            };

            let token = jwtSimple.encode(payload, jwt.jwtSecret);

            return { jwt_token: token };
        }

        let payload: JWTPayload = {
            user_id: registerEmail['id'],
            username: registerEmail['username'],
        };

        let token = jwtSimple.encode(payload, jwt.jwtSecret);

        return { jwt_token: token };
    }

    async localLogin(
        { username, password }: NativeLoginInput,
        knex = this.knex,
    ): Promise<NativeLoginOutput> {
        if (!jwt.jwtSecret) {
            throw new HttpException(500, "Internal Service Error - missing 'JWT_Secret' in env.");
        }

        const row = await knex(tables.USERS)
            .select('id', 'username', 'password')
            .where({ username })
            .first();

        if (!row) {
            throw new HttpException(401, 'Incorrect username/password');
        }

        const isMatch = await checkPassword(password, row.password);

        if (!isMatch) {
            throw new HttpException(401, 'Incorrect username/password');
        }

        let payload: JWTPayload = {
            user_id: row.id,
            username: row.username,
        };

        let token = jwtSimple.encode(payload, jwt.jwtSecret);

        return { jwt_token: token };
    }
}
