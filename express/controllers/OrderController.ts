import { BaseController, HttpException } from '../base-controller';
import { OrderService } from '../services/OrderService';

// interface
import { ICustomerOrderInput } from '../interface/order';

export class OrderController extends BaseController {
    constructor(public orderService: OrderService) {
        super();
    }

    receivedOrder = this.handleRequest(async (req, res) => {
        const { customerName, customerAddress, productId, quantity }: ICustomerOrderInput =
            req.body;

        if (
            (typeof customerName || typeof customerAddress) !== 'string' ||
            customerName.length > 50 ||
            customerAddress.length > 300
        ) {
            throw new HttpException(400, 'Invalid Input');
        }

        if ((typeof productId || typeof quantity) !== 'number') {
            throw new HttpException(400, 'Invalid Input');
        }

        await this.orderService.create({
            customerName,
            customerAddress,
            productId,
            quantity,
        });
    });
}
