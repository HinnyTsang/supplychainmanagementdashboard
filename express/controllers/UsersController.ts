import { BaseController } from '../base-controller';
import { HttpException } from '../base-controller';
import { UsersService } from '../services/UsersService';
import { OAuth2Client, TokenPayload } from 'google-auth-library';

export class UsersController extends BaseController {
    constructor(public usersService: UsersService) {
        super();
    }

    login = this.handleRequest(async (req, res) => {
        const { username, password } = req.body;
        if (!username) {
            throw new HttpException(400, 'Missing username');
        }

        if (!password) {
            throw new HttpException(400, 'Missing password');
        }
        return await this.usersService.localLogin({ username, password });
    });

    googleLogin = this.handleRequest(async (req, res) => {
        const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_CLIENT_ID);
        if (!client) {
            throw new HttpException(500, 'Missing Google Client ID');
        }
        const { token } = req.body;
        if (!token && token.length > 100) {
            throw new HttpException(400, 'Missing token');
        }
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: process.env.CLIENT_ID,
        });
        const googleToken: TokenPayload | undefined = ticket.getPayload();
        const name = googleToken?.name;
        const email = googleToken?.email;
        const picture = googleToken?.picture;
        if (name && email && picture) {
            return await this.usersService.googleLogin({ name, email, picture });
        }
        throw new HttpException(500, 'Internal Server Error');
    });
}
