import { BaseController } from '../base-controller';
import { ProductService } from '../services/ProductService';
import { HttpException } from '../base-controller';

export class ProductController extends BaseController {
    constructor(public productService: ProductService) {
        super();
    }

    create = this.handleRequest(async (req, res) => {
        const { productName, quantity, supplierId } = req.body;

        if (typeof productName !== 'string') {
            throw new HttpException(400, 'Invalid Input');
        }

        if (typeof quantity !== 'number') {
            throw new HttpException(400, 'Invalid Input');
        }

        if (productName.length > 50) {
            throw new HttpException(400, 'Invalid Input');
        }

        if (quantity > 1000) {
            throw new HttpException(400, 'Invalid Input');
        }

        if (typeof supplierId !== 'number') {
            throw new HttpException(400, 'Invalid Input');
        }

        if (supplierId > 100000) {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.productService.create({ productName, quantity, supplierId });
    });

    read = this.handleRequest(async (req, res) => {
        return await this.productService.read();
    });
}
