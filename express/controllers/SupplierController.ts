import { BaseController } from '../base-controller';
import { HttpException } from '../base-controller';
import { SupplierService } from '../services/SupplierService';

export class SupplierController extends BaseController {
    constructor(public supplierSerivce: SupplierService) {
        super();
    }

    create = this.handleRequest(async (req, res) => {
        const { name, contact } = req.body;
        if (!name && contact) {
            throw new HttpException(400, 'Invalid Input');
        }

        if (name.length > 100 || contact.length > 20) {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.supplierSerivce.create({ name, contact });
    });

    update = this.handleRequest(async (req, res) => {
        const { id, name, contact } = req.body;

        if (!id) {
            throw new HttpException(400, 'Invalid Input');
        }

        if (name && contact) {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.supplierSerivce.update({ id, name, contact });
    });

    read = this.handleRequest(async (req, res) => {
        return await this.supplierSerivce.read();
    });

    readSingle = this.handleRequest(async (req, res) => {
        const { id } = req.params;

        if (typeof +id !== 'number') {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.supplierSerivce.readSingle(+id);
    });

    delete = this.handleRequest(async (req, res) => {
        const { id } = req.body;

        if (!id) {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.supplierSerivce.delete(id);
    });
}
