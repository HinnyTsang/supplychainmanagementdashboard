import { BaseController, HttpException } from '../base-controller';
import { ShippmentService } from '../services/ShippmentService';

// interface
import { IDeliveryFromSupplierInput } from '../interface/shippment';

// redis
import { client } from '../lib/redis';

// updateProductDashboard function
import { updateProductDashboard } from '../lib/updateProductDashboard'

export class ShippmentController extends BaseController {
    constructor(public shippmentService: ShippmentService) {
        super();
    }

    // allShipmentRecords = this.handleRequest(async (req, res) => {
    //     return await this.shippmentService.allRecords();
    // });

    processToNextWorkStation = this.handleRequest(async (req, res) => {
        const { currentWorkstation, selectedShipmentIds } = req.body;

        if (!Array.isArray(selectedShipmentIds) || typeof currentWorkstation !== 'string') {
            throw new HttpException(400, 'Invalid Input');
        }

        const data =  await this.shippmentService.processNextWorkStation({
            currentWorkstation,
            selectedShipmentIds,
        });

        const result = await updateProductDashboard()
        await client.connect().catch(error => {})
        await client.setEx('product-dashboard', 600, JSON.stringify(result))
        await client.quit()

        return data
    });

    getWorkstationList = this.handleRequest(async (req, res) => {
        const workStation = req.params['workstation'];

        if (workStation.length > 30) {
            throw new HttpException(400, 'Invalid Input');
        }

        return await this.shippmentService.getWorkstationList({ workStation });
    });

    deliveryGoodsFromSupplier = this.handleRequest(async (req, res) => {
        const { supplierId, productId, quantity }: IDeliveryFromSupplierInput = req.body;

        if (
            typeof supplierId !== 'number' ||
            typeof productId !== 'number' ||
            typeof quantity !== 'number'
        ) {
            throw new HttpException(400, 'Invalid Input');
        }

        const data =  await this.shippmentService.deliveryGoodsFromSupplier({
            supplierId,
            productId,
            quantity,
        })

        const result = await updateProductDashboard()
        await client.connect().catch(error => {})
        await client.setEx('product-dashboard', 600, JSON.stringify(result))
        await client.quit()

        return data
    });
}
