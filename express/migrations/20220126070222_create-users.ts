import { Knex } from 'knex';
import tables from '../interface/tables';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tables.USERS);

    if (!hasTable) {
        await knex.schema.createTable(tables.USERS, (table) => {
            table.increments();
            table.string('username').notNullable();
            table.string('email').nullable();
            table.string('password').notNullable();
            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tables.USERS);
}
