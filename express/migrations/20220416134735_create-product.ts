import { Knex } from 'knex';
import tables from '../interface/tables';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tables.PRODUCT);

    if (!hasTable) {
        await knex.schema.createTable(tables.PRODUCT, (table) => {
            table.increments();
            table.string('product_name').notNullable();
            table.string('status').notNullable();
            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tables.PRODUCT);
}
