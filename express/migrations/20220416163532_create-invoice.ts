import { Knex } from 'knex';
import tables from '../interface/tables';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tables.INVOICE);

    if (!hasTable) {
        await knex.schema.createTable(tables.INVOICE, (table) => {
            table.increments();

            table.string('customer_name').notNullable();
            table.string('customer_address').notNullable();

            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {}
