import { Knex } from 'knex';
import tables from '../interface/tables';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable(tables.SUPPLIER);

    if (!hasTable) {
        await knex.schema.createTable(tables.SUPPLIER, (table) => {
            table.increments();

            table.string('name').notNullable();
            table.string('contact').notNullable();

            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(tables.SUPPLIER);
}
