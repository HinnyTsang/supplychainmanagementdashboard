import express from 'express';

// controller
import { UsersController } from './controllers/UsersController';
import { ShippmentController } from './controllers/ShippmentController';
import { SupplierController } from './controllers/SupplierController';
import { ProductController } from './controllers/ProductController';
import { OrderController } from './controllers/OrderController';

import { requireLogin } from './lib/guards';

export default function (args: {
    usersController: UsersController;
    shippmentController: ShippmentController;
    supplierController: SupplierController;
    productController: ProductController;
    orderController: OrderController;
}) {
    let router = express.Router();

    // Users Route
    router.post('/login', args.usersController.login);

    // Google Login
    router.post('/google-login', args.usersController.googleLogin);

    // Supplier Route
    router.get('/supplier/:id', requireLogin, args.supplierController.readSingle);
    router.get('/supplier', requireLogin, args.supplierController.read);
    router.post('/supplier', requireLogin, args.supplierController.create);
    router.put('/supplier', requireLogin, args.supplierController.update);
    router.delete('/supplier', requireLogin, args.supplierController.delete);

    // Product Route
    router.get('/product', requireLogin, args.productController.read);
    router.post('/product', requireLogin, args.productController.create);

    // Shippment Route
    // router.get('/shippment', requireLogin, args.shippmentController.allShipmentRecords);
    router.post(
        '/delivery-form-supplier',
        requireLogin,
        args.shippmentController.deliveryGoodsFromSupplier,
    );

    // Order Route
    router.post('/order', requireLogin, args.orderController.receivedOrder);

    // WorkStation Route
    router.get(
        '/workstation-list/:workstation',
        requireLogin,
        args.shippmentController.getWorkstationList,
    );
    router.post(
        '/workstation-process',
        requireLogin,
        args.shippmentController.processToNextWorkStation,
    );

    return router;
}
