import { Knex } from 'knex';
import tables from '../interface/tables';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.SUPPLIER).del();

    // Inserts seed entries
    await knex(tables.SUPPLIER).insert([{ name: '佢玩哂供應商', contact: '62717171' }]);
}
