import { Knex } from 'knex';
import tables from '../interface/tables';
import { hashPassword } from '../lib/hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(tables.USERS).del();

    const hashedPassword = await hashPassword('12333211');

    // Inserts seed entries
    await knex(tables.USERS).insert([
        {
            username: 'happy',
            password: hashedPassword,
        },
    ]);
}
